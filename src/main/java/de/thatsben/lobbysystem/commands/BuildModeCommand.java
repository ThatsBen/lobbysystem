package de.thatsben.lobbysystem.commands;

import de.thatsben.lobbysystem.LobbySystem;
import de.thatsben.lobbysystem.utils.Msg;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class BuildModeCommand implements CommandExecutor {

    private List<UUID> buildPlayerList = new ArrayList<>();
    private final String PERMISSION = "bs.cmd.build";
    private final String PERMISSION_OTHER = "bs.cmd.buld.other";

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(LobbySystem.getInstance().getPrefix() + "Du musst ein Spieler sein.");
        }

        Player player = (Player) sender;
        if (!player.hasPermission(PERMISSION)) {
            player.sendMessage(Msg.noPermissions);
        }

        if (args.length == 0)
        {
            Inventory lastPlayerInventory = player.getInventory(); // TODO: implement this
            if (buildPlayerList.contains(player.getUniqueId()))
            {
                // remove player from the list and set the adventure gamemode
                buildPlayerList.remove(player.getUniqueId());
                player.setGameMode(GameMode.ADVENTURE);
                player.sendMessage(LobbySystem.getInstance().getPrefix() + ChatColor.RED + "Du bist nun nicht mehr im BuildMode!");
            }
            else
            {
                // add the player to the list and set the creative gamemode
                buildPlayerList.add(player.getUniqueId());
                player.setGameMode(GameMode.CREATIVE);
                player.sendMessage(LobbySystem.getInstance().getPrefix() + ChatColor.GRAY + "Du bist nun im BuildMode!");
            }
            return true;
        }

        if (args.length == 1)
        {
            if (player.hasPermission(PERMISSION_OTHER)) {
                Player target = Bukkit.getPlayer(args[0]);
                if (target == null) {
                    player.sendMessage(Msg.notOnline);
                    return true;
                }

                Inventory lastTargetInventory = target.getInventory(); // TODO: implement this

                if (buildPlayerList.contains(target.getUniqueId())) {
                    // remove the target from the list and set the adventure gamemode and send the sender a message
                    buildPlayerList.remove(target.getUniqueId());
                    target.setGameMode(GameMode.ADVENTURE);
                    target.sendMessage(Msg.systemPrefix + "Du bist nun nicht mehr im BuildMode!");

                    player.sendMessage(Msg.systemPrefix + ChatColor.GRAY + "Der Spieler " + ChatColor.YELLOW + target.getDisplayName() + ChatColor.GRAY + " ist nun" + ChatColor.RED + "nicht mehr" + ChatColor.GRAY + "im BuildMode!");

                } else {
                    // add the target to the list and set the creative gamemode and send the sender a message
                    buildPlayerList.add(target.getUniqueId());
                    target.setGameMode(GameMode.CREATIVE);
                    target.sendMessage(LobbySystem.getInstance().getPrefix() + ChatColor.GRAY + "Du bist nun im BuildMode!");
                    player.sendMessage(LobbySystem.getInstance().getPrefix() + ChatColor.GRAY + "Der Spieler " + ChatColor.YELLOW + target.getDisplayName() + ChatColor.GRAY + " ist nun im BuildMode!");
                }
                return true;
            } else {
                player.sendMessage(LobbySystem.getInstance().getUsagePrefix() + "/build [Name]");
            }
            }
        return false;
    }
}
