package de.thatsben.lobbysystem.commands;

import de.thatsben.lobbysystem.LobbySystem;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class InfoCommand implements LobbySystemCommand {

    private LobbySystem plugin;

    public InfoCommand(LobbySystem plugin) {
        this.plugin = plugin;
    }

    @Override
    public String getName()
    {
        return "info";
    }

    @Override
    public String[] getAliases()
    {
        return new String[0];
    }

    @Override
    public String getPermission()
    {
        return "lobbysystem.infocommand";
    }

    @Override
    public String getUsage()
    {
        return "§4Verwendung §8» §c/info <Admin/Dev/SrContent/SrMod/Mod/Sup/Content/Builder/Twitter/Insta/Shop>";
    }

    /**
     * this method outputs a few helpful support messages
     */
    @Override
    public boolean onExecute(Player player, String[] args)
    {
        if (args.length != 1)
        {
            getUsage();
        }

        switch (args[0].toLowerCase())
        {
            case "admin":
                Bukkit.broadcastMessage("§4Admin §7steht für §4Administrator§7. Die §4Administratoren §7bilden die Serverleitung und kümmern sich um diverse Aufgabenfelder.");
                break;
            case "dev":
                Bukkit.broadcastMessage("§bDev §7steht für §bDeveloper§7. Die §bDeveloper §7sind für die Entwicklung und Wartung aller Komponenten verantwortlich.");
                break;
            case "srcontent":
                Bukkit.broadcastMessage("§cSrC §7steht für §cSenior-Content§7. Die §cSenior-Contents §7bilden zusammen die Teamleitung der Contents."); //update
                break;
            case "srmod":
                Bukkit.broadcastMessage("§2SrMod §7steht für §2Senior-Moderator§7. Sie bilden zusammen mit einem §4Admin §7die Teamleitung der Moderation.");
                break;
            case "mod":
                Bukkit.broadcastMessage("§2Mod §7steht für §2Moderator§7. Die §2Moderatoren §7kümmern sich um allgemeine Anliegen, die das Netzwerk und dessen Komponenten betreffen.");
                break;
            case "sup":
                Bukkit.broadcastMessage("§aSup §7steht für §aSupporter§7. Die §aSupporter §7sind Ansprechpartner für Fragen und Probleme.");
                break;
            case "content":
                Bukkit.broadcastMessage("§cC §7steht für §cContent§7. Die §cContents §7sind für vielseitige Bereiche zuständig, die für die Weiterentwicklung und Instandhaltung des Netzwerkes notwendig sind.");
                break;
            case "twitter":
                Bukkit.broadcastMessage("§bTwitter");
                break;
            case "instagram":
                Bukkit.broadcastMessage("§cInstagram");
                break;
            case "shop":
                Bukkit.broadcastMessage("§eShop");
                break;
            default:
                player.sendMessage("§4Verwendung §8» §c/info <Admin/Dev/SrContent/SrMod/Mod/Sup/Content/Builder/Twitter/Insta/Shop>");
        }
        return false;
    }
}
