package de.thatsben.lobbysystem.commands;

import de.thatsben.lobbysystem.utils.Msg;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class FlyCommand implements CommandExecutor {

    private final String PERMISSION = "bs.cmd.fly";
    private final String PERMISSION_OTHER = "cs.cmd.fly.other";

    private List<UUID> flyPlayerList = new ArrayList<>();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            System.out.println("Du musst ein Spieler sein.");
            return true;
        }

        Player player = (Player) sender;
        if (!player.hasPermission(PERMISSION)) {
            player.sendMessage(Msg.noPermissions);
            return true;
        }

        if (args.length == 0) {
            if (flyPlayerList.contains(player.getUniqueId())) {
                // remove player from the list and disallow to fly
                flyPlayerList.remove(player.getUniqueId());
                player.setAllowFlight(false);
                player.sendMessage(Msg.systemPrefix + "§cDu kannst nun nicht mehr fliegen!");
            } else {
                // add the player to the list and allow flying
                flyPlayerList.add(player.getUniqueId());
                player.setAllowFlight(true);
                player.sendMessage(Msg.systemPrefix + "§7Du kannst nun fliegen!");
            }
            return true;
        }

        if (args.length == 1) {
            if (player.hasPermission(PERMISSION_OTHER)) {
                Player target = Bukkit.getPlayer(args[0]);
                if (target == null) {
                    player.sendMessage(Msg.notOnline);
                    return true;
                }

                if (flyPlayerList.contains(target.getUniqueId())) {
                    // remove the target from the list and disallow to fly and send the sender a message
                    flyPlayerList.remove(target.getUniqueId());
                    target.setAllowFlight(false);
                    target.sendMessage(Msg.systemPrefix + "§cDu kannst nun nicht mehr fliegen!");
                    player.sendMessage(Msg.systemPrefix + "§7Der Spieler §e" + target.getDisplayName() + " §7kann nun nicht mehr fliegen");
                } else {
                    // add the target to the list and allow for fly and send the sender a message
                    flyPlayerList.add(target.getUniqueId());
                    target.setAllowFlight(true);
                    target.sendMessage(Msg.systemPrefix + "§7Du kannst nun fliegen!");
                    player.sendMessage(Msg.systemPrefix + "§7Der Spieler §e" + target.getDisplayName() + " §7kann nun fliegen!");
                }
                return true;

            } else {
                player.sendMessage(Msg.usagePrefix + "/fly [Player]");
            }
        }
        return false;
    }
}
