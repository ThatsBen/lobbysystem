package de.thatsben.lobbysystem.commands;

import de.thatsben.lobbysystem.LobbySystem;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class TeleportCommand implements LobbySystemCommand {

    private LobbySystem plugin;

    public TeleportCommand(LobbySystem plugin)
    {
        this.plugin = plugin;
    }

    @Override
    public String getName()
    {
        return "tp";
    }

    @Override
    public String[] getAliases()
    {
        return new String[0];
    }

    @Override
    public String getPermission()
    {
        return "lobbysystem.teleportcommand";
    }

    @Override
    public String getUsage()
    {
        return "<Spielername>";
    }

    /**
     * This method teleport one player to another player
     */
    @Override
    public boolean onExecute(Player player, String[] args)
    {
        if (args.length == 1)
        {
            Player target = Bukkit.getPlayer(args[0]);
            if (target == null)
            {
                player.sendMessage(plugin.getPrefix() + ChatColor.RED +  "Der Spieler ist nicht online!");
                return true;
            }

            player.teleport(target);
            player.sendMessage(plugin.getPrefix() + ChatColor.GRAY + "Du wurdest erfolgreich zu " 
            + ChatColor.YELLOW + target.getDisplayName() + ChatColor.GRAY + " teleportiert.");
            return true;
        }
        else 
        {
            player.sendMessage(getUsage());
        }
        return false;
    }
}
