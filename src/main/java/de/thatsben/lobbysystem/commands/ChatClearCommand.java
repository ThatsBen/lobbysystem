package de.thatsben.lobbysystem.commands;

import de.thatsben.lobbysystem.utils.Msg;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ChatClearCommand implements CommandExecutor {

    private final String PERMISSION = "bs.cmd.chatclear";
    private final String PERMISSION_NOTIFY = "bs.cmd.chatclear.notify";

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(Msg.onlyForPlayer);
            return true;
        }

        Player player = (Player) sender;
        if (!player.hasPermission(PERMISSION)) {
            player.sendMessage(Msg.noPermissions);
            return true;
        }

        if (args.length != 0) {
            player.sendMessage(Msg.usagePrefix + "/chearclear");
            return true;
        }

        for (int i = 0; i < 105; i++)
        {
            for (Player players : Bukkit.getOnlinePlayers())
            {
                if (!players.hasPermission(PERMISSION))
                {
                    players.sendMessage(" ");
                }
            }
        }

        Bukkit.broadcastMessage(Msg.systemPrefix + ChatColor.GRAY + "Der Chat wurde von " + ChatColor.YELLOW + player.getDisplayName() + ChatColor.GRAY + " geleert!");
        for (Player notifyPlayer : Bukkit.getOnlinePlayers()) {
            if (notifyPlayer.hasPermission(PERMISSION_NOTIFY)) {
                notifyPlayer.sendMessage(Msg.info + "§e" + player.getDisplayName() + " §7hat den Chat auf §eServerxy §7geleert."); //TODO: implement proxy
            }
        }

        return false;
    }
}
