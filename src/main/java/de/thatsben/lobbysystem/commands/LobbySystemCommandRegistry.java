package de.thatsben.lobbysystem.commands;

import com.google.common.collect.Lists;
import de.thatsben.lobbysystem.LobbySystem;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.List;

@Deprecated
public class LobbySystemCommandRegistry implements CommandExecutor {

    // -- instances
    
    private final LobbySystem plugin;
    private final List<LobbySystemCommand> commands;

    // -- constructor
    
    public LobbySystemCommandRegistry(final LobbySystem plugin)
    {
        this.plugin = plugin;
        this.commands = Lists.newArrayList();
    }

    // -- public methods
    
    /**
     * This method adds the commands that are inserted as parameters to the commands list
     * 
     * @param lobbySystemCommand - classes (commands) which implement the LobbySystemCommand interface
     */
    public void register(final LobbySystemCommand lobbySystemCommand)
    {
        this.commands.add(lobbySystemCommand);
    }

    /**
     * this method registers all commands with the register method
     */
    public void init()
    {
        this.register(new TeleportCommand(this.plugin));
        this.register(new InfoCommand(this.plugin));
    }

    /**
     * This method executes the commands that implement the LobbySystemCommand interface and are in the commands list.
     * First it is checked if the sender is a player and if the argument length is 1. 
     * Then all commands are passed through in a loop. There it is checked if the first argument matches one of the command names or aliases. 
     */
    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String s, final String[] args)
    {
        if (!(sender instanceof Player))
        {
            System.out.println(this.plugin.getPrefix() + ChatColor.RED + "Du musst ein Spieler sein.");
            return false;
        }
        
        final Player player = (Player) sender;
        if (args.length == 0) 
        {
            this.sendHelp(player);
            return false;
        }
        
        if (args.length == 1)
        {
            for (final LobbySystemCommand lobbySystemCommand : this.commands)
            {
                if (args[0].equalsIgnoreCase(lobbySystemCommand.getName()) || Arrays.asList(lobbySystemCommand.getAliases()).contains(args[0]))
                {
                    if (!player.hasPermission(lobbySystemCommand.getPermission()))
                    {
                        this.sendNoPerms(player);
                        return false;
                    }
                    if (!lobbySystemCommand.onExecute(player, Arrays.copyOfRange(args, 1, args.length)))
                    {
                        player.sendMessage(this.plugin.getPrefix() + ChatColor.RED + "Benutzung:" + ChatColor.GRAY + " /lobbysystem " + lobbySystemCommand.getName() + ChatColor.WHITE + lobbySystemCommand.getUsage());
                    }
                    return true;
                }
            }   
        }
        this.sendHelp(player);
        return false;
    }
    
    // -- private methods

    /**
     * This method first checks if you have permissions for at least one command. 
     * If the player has at least one permission for a command, then the system loops through all commands in the commands list
     * and checks whether the required permissions for the current command are available.
     * For all commands for which the player has the permission, the player gets a message with the command and the usage
     * 
     * @param player who gets the message
     */
    private void sendHelp(final Player player)
    {
        boolean permissions = false;
        for (final LobbySystemCommand lobbySystemCommand : this.commands)
        {
            if (player.hasPermission(lobbySystemCommand.getPermission()))
            {
                permissions = true;
            }
        }
        
        if (permissions)
        {
            player.sendMessage(this.plugin.getPrefix() + "Übersicht der Befehle");
            for (final LobbySystemCommand lobbySystemCommand : this.commands)
            {
                if (player.hasPermission(lobbySystemCommand.getPermission()))
                {
                    player.sendMessage(ChatColor.GRAY + "- /lobbysystem " + lobbySystemCommand.getName() + ChatColor.YELLOW + lobbySystemCommand.getUsage());
                }
            }
        }
        else
        {
            this.sendNoPerms(player);
        }
    }

    /**
     * This method sends the player specified in the parameters a message that he has no permissions 
     * 
     * @param player who gets the message
     */
    private void sendNoPerms(final Player player)
    {
        player.sendMessage(this.plugin.getPrefix() + ChatColor.RED + "Du hast keine Berechtigung für diesen Befehl.");
    }
}

