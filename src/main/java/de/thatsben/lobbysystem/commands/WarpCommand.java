package de.thatsben.lobbysystem.commands;

import de.thatsben.lobbysystem.LobbySystem;
import de.thatsben.lobbysystem.utils.Msg;
import de.thatsben.lobbysystem.warp.WarpManager;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class WarpCommand implements CommandExecutor {

    private final String PERMISSION = "bs.cmd.warp";

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(Msg.onlyForPlayer);
            return true;
        }

        Player player = (Player) sender;
        if (!player.hasPermission(PERMISSION)) {
            player.sendMessage(Msg.noPermissions);
            return true;
        }

        if (args.length == 1)
        {
            if (WarpManager.getWarp(args[0]) == null)
            {
                player.sendMessage(Msg.systemPrefix + ChatColor.RED +  "Dieser Warp existiert nicht!");
                return true;
            }
            player.teleport(WarpManager.getWarp(args[0]));
            return true;
        }

        if (args.length == 2)
        {
            if (args[0].equalsIgnoreCase("add"))
            {
                if (WarpManager.getWarp(args[1]) == null)
                {
                    WarpManager.createWarp(args[1], player.getLocation());
                    player.sendMessage(Msg.systemPrefix + ChatColor.GRAY + "Du hast erfolgreich den Warp " + ChatColor.YELLOW + args[1] + ChatColor.GRAY + " erstellt.");
                    return true;
                }
                player.sendMessage(Msg.systemPrefix + ChatColor.RED + "Dieser Warp existiert bereits.");
                return true;
            }

            if (args[0].equalsIgnoreCase("delete"))
            {
                if (WarpManager.getWarp(args[1]) == null)
                {
                    player.sendMessage(Msg.systemPrefix + ChatColor.RED + "Dieser Warp existiert nicht!");
                    return true;
                }
                WarpManager.deleteWarp(args[1]);
                player.sendMessage(Msg.systemPrefix + ChatColor.GRAY + "Du hast erfolgreich den Warp " + ChatColor.YELLOW + args[1] + ChatColor.GRAY + " gelöscht.");
                return true;
            }
        }
        return false;
    }
}
