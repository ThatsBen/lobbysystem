package de.thatsben.lobbysystem.commands;

import org.bukkit.entity.Player;

public interface LobbySystemCommand
{
    String getName();
    String[] getAliases();
    String getPermission();
    String getUsage();
    boolean onExecute(Player player, String[] args);
}
