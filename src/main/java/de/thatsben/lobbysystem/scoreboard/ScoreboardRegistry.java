package de.thatsben.lobbysystem.scoreboard;

import de.thatsben.lobbysystem.LobbySystem;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

public class ScoreboardRegistry implements Listener
{

    private LobbySystem plugin;

    public ScoreboardRegistry(LobbySystem plugin)
    {
        this.plugin = plugin;
    }

    public Scoreboard get(Player player)
    {
        Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        Objective objective = scoreboard.registerNewObjective("aaa", "dummy");

        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        objective.setDisplayName("§e§lTHATSBEN.DE");

        objective.getScore("§e").setScore(15);
        objective.getScore("§1").setScore(14);
        objective.getScore("§2").setScore(13);
        objective.getScore("§3").setScore(12);
        objective.getScore("§4").setScore(11);
        objective.getScore("§5").setScore(10);
        objective.getScore("§6").setScore(9);

        player.setScoreboard(scoreboard);
        return scoreboard;
    }

    
    @EventHandler
    public void onJoin(PlayerJoinEvent event)
    {
        Player player = event.getPlayer();
        player.setScoreboard(get(player));
    }
}
