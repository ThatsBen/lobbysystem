package de.thatsben.lobbysystem;

import de.thatsben.lobbysystem.broadcast.BroadcastMessageTask;
import de.thatsben.lobbysystem.commands.BuildModeCommand;
import de.thatsben.lobbysystem.commands.ChatClearCommand;
import de.thatsben.lobbysystem.commands.FlyCommand;
import de.thatsben.lobbysystem.commands.WarpCommand;
import de.thatsben.lobbysystem.scoreboard.ScoreboardRegistry;
import de.thatsben.lobbysystem.utils.Config;
import de.thatsben.lobbysystem.listener.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;

public class LobbySystem extends JavaPlugin
{

    private static LobbySystem instance;

    /**
     * this method executes the startup logic
     */
    @Override
    public void onEnable()
    {
        final long epoch = System.currentTimeMillis();
        Bukkit.getLogger().info("Initializing lobbysystem...");

        instance = this;

        this.loadConfig();
        this.initVariables();
        this.initListener();
        this.initScoreboard();
        this.initBroadcastMessage();
        this.initConfig();
        this.registerCommand();

        final long initializeDuration = System.currentTimeMillis() - epoch;
        Bukkit.getLogger().info(String.format("Successfully initialized lobbysystem plugin. (took %sms)", initializeDuration));
    }

    /**
     * this method executes the shutdown logic
     */
    @Override
    public void onDisable()
    {
        Bukkit.getLogger().info("Successfully disabled lobbysystem plugin.");
    }

    // -- config

    private File file;
    private YamlConfiguration config;

    /**
     * This method creates a config and enters a some default values
     */
    private void loadConfig() {
        if (!getDataFolder().exists())
        {
            getDataFolder().mkdirs();
        }

        file = new File(getDataFolder(), "config.yml");
        if (!file.exists())
        {
            try
            {
                file.createNewFile();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }

        config = YamlConfiguration.loadConfiguration(file);
        config.options().copyDefaults(true);
        config.addDefault("prefix", "&8[&eSystem&8] ");
        config.addDefault("errorPrefix", "&4Fehler &8» &c");
        config.addDefault("usagePrefix", "&4Verwendung &8» &c");
        config.addDefault("broadcastCooldown", 60);

        try
        {
            config.save(file);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    
    private String prefix;
    private String errorPrefix;
    private String usagePrefix;
    private int broadcastCooldown;

    /**
     * this method initializes the variables for the config
     */
    private void initVariables()
    {
        prefix = ChatColor.translateAlternateColorCodes('&', this.getConfig().getString("prefix"));
        errorPrefix = ChatColor.translateAlternateColorCodes('&', this.getConfig().getString("errorPrefix"));
        usagePrefix = ChatColor.translateAlternateColorCodes('&', this.getConfig().getString("usagePrefix"));
        broadcastCooldown = this.getConfig().getInt("broadcastCooldown");
    }

    // -- config for the warp system

    private static Config cfg;

    /**
     * this method initializes the config
     */
    private void initConfig()
    {
        cfg = new Config("locations.yml", getDataFolder());
    }

    // -- broadcast task 

    /**
     * this method initializes the broadcast message task
     */
    private void initBroadcastMessage()
    {
        BroadcastMessageTask broadcastMessageTask = new BroadcastMessageTask();
        broadcastMessageTask.startMessageTask();
    }

    // -- event listener register

    /**
     * this method initializes the listener
     */
    private void initListener()
    {
        PluginManager pl = Bukkit.getPluginManager();

        pl.registerEvents(new CancelListener(), this);
        pl.registerEvents(new HidePlayerListener(), this);
        pl.registerEvents(new ChatListener(), this);
        pl.registerEvents(new DoubleJumpListener(), this);
        pl.registerEvents(new CompassListener(), this);
        pl.registerEvents(new LobbySwitchListener(), this);
        pl.registerEvents(new PlayerJoinListener(), this);
        pl.registerEvents(new PlayerQuitListener(), this);
        //pl.registerEvents(new NicknameItemListener(), this);
        //pl.registerEvents(new BuildBlockPlaceListener(), this);
    }

    // -- command register

    private void registerCommand() {
        getCommand("build").setExecutor(new BuildModeCommand());
        getCommand("fly").setExecutor(new FlyCommand());
        getCommand("chatclear").setExecutor(new ChatClearCommand());
        getCommand("warp").setExecutor(new WarpCommand());
    }

    // -- scoreboard
    
    private ScoreboardRegistry scoreboardRegistry;

    /**
     * this method initializes the scoreboard
     */
    private void initScoreboard()
    {
        scoreboardRegistry = new ScoreboardRegistry(this);
        Bukkit.getPluginManager().registerEvents(scoreboardRegistry, this);
    }
    
    // -- getter and setter
    
    public static LobbySystem getInstance()
    {
        return instance;
    }

    public String getPrefix()
    {
        return prefix;
    }

    public String getErrorPrefix() {
        return errorPrefix;
    }

    public String getUsagePrefix() {
        return usagePrefix;
    }

    public int getBroadcastCooldown()
    {
        return broadcastCooldown;
    }
    
    public static Config getCfg()
    {
        return cfg;
    }
}
