package de.thatsben.lobbysystem.broadcast;

import de.thatsben.lobbysystem.LobbySystem;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Random;

public class BroadcastMessageTask
{

    // prefix which is placed at the beginning of a message before the broadcasts
    private final String broadcastPrefix = "§8[§eBroadcast§8] ";

    // in this string array all broadcast texts are stored
    private final String[] broadcastMessagesArray = {
            "§7Test 1",
            "§7Test 2",
            "§7Test 3"
    };

    /**
     * this method starts the broadcast message task.
     * It will then randomly select messages from the broadcastMessageArray after a specified interval.
     */
    public void startMessageTask()
    {
        new BukkitRunnable()
        {
            @Override
            public void run()
            {
                Random random = new Random();
                int stringArrayIndex = random.nextInt(broadcastMessagesArray.length);
                Bukkit.getOnlinePlayers().forEach(players -> players.sendMessage(broadcastPrefix + broadcastMessagesArray[stringArrayIndex]));
            }
        }.runTaskTimer(LobbySystem.getInstance(), 5 * 20L, LobbySystem.getInstance().getConfig().getInt("broadcastCooldown") * 20L);
    }

}
