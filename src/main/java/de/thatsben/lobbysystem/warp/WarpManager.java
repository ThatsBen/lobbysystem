package de.thatsben.lobbysystem.warp;

import de.thatsben.lobbysystem.LobbySystem;
import org.bukkit.Location;

public class WarpManager
{

    /**
     * 
     * @param name - Name of the warp
     * @return the location of the warp you requested
     */
    public static Location getWarp(String name)
    {
        return (Location) LobbySystem.getCfg().getConfiguration().get(name);
    }

    /**
     * This method creates a warp with a name and a location
     * @param name - the name of the warp 
     * @param location - The location to which the warp points back
     */
    public static void createWarp(String name, Location location)
    {
        LobbySystem.getCfg().set(name, location);
        LobbySystem.getCfg().save();
    }

    /**
     * this method deletes a warp 
     * @param name - the warp to be deleted
     */
    public static void deleteWarp(String name)
    {
        LobbySystem.getCfg().set(name, null);
        LobbySystem.getCfg().save();
    }

}
