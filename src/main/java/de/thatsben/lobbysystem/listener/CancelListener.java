package de.thatsben.lobbysystem.listener;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

public class CancelListener implements Listener {

    /**
     * This boolean checks if the player is in adventure gamemode to check if the event should be canceled or not.
     * @param player where it is checked in which gamemode it is.
     * @return true when the player is in adventure mode
     */
    private boolean isPlayerInAdventureMode(Player player)
    {
        return player.getGameMode().equals(GameMode.ADVENTURE);
    }

    @EventHandler
    public void onDrop(PlayerDropItemEvent event)
    {
        if (isPlayerInAdventureMode(event.getPlayer()))
        {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPickUpItem(PlayerPickupItemEvent event)
    {
        if (isPlayerInAdventureMode(event.getPlayer()))
        {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onWeatherChange(WeatherChangeEvent event)
    {
        event.setCancelled(true);
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event)
    {
        Player player = (Player) event.getWhoClicked();
        if (isPlayerInAdventureMode(player))
        {
            event.setCancelled(true);
        }
    }

    /*
    @EventHandler
    public void onPlace(BlockPlaceEvent event)
    {
        if (isPlayerInAdventureMode(event.getPlayer()))
        {
            event.setCancelled(true);
        }
    }
     */

    @EventHandler
    public void onBreak(BlockBreakEvent event)
    {
        if (isPlayerInAdventureMode(event.getPlayer()))
        {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onDamage(EntityDamageEvent event)
    {
        event.setCancelled(true);
    }

    @EventHandler
    public void onFoodLevelChange(FoodLevelChangeEvent event)
    {
        event.setCancelled(true);
    }

}
