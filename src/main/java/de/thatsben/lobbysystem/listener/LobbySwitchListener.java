package de.thatsben.lobbysystem.listener;

import de.thatsben.lobbysystem.utils.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;

public class LobbySwitchListener implements Listener
{

    /**
     * This method creates an inventory
     * @return inventory which the player gets when he wants to change the lobby
     */
    public Inventory get()
    {
        Inventory inventory = Bukkit.createInventory(null, 9 * 3, "§8Welche Lobby soll es sein?");

        for (int i = 0; i < inventory.getSize(); i++)
        {
            inventory.setItem(i, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short) 7).withName(" ").build());
        }

        return inventory;
    }

    /**
     * This method opens the inventory with a right click
     * @param event
     */
    @EventHandler
    public void onInteract(PlayerInteractEvent event)
    {
        Player player = event.getPlayer();
        if (event.getItem() == null) return;
        if (event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_AIR)
        {
            if (event.getItem().getItemMeta().getDisplayName().equals("§6Lobby Auswahl"))
            {
                player.openInventory(get());
            }
        }
    }

    /**
     * TODO: work on this method
     * This method includes the lobby change logic
     * @param event
     */
    @EventHandler
    public void onInventoryClick(InventoryClickEvent event)
    {
        Player player = (Player) event.getWhoClicked();
        if (event.getView().getTitle().equals("§8Welche Lobby soll es sein?"))
        {
            event.setCancelled(true);

            // lobby switch logic

        }
    }

}
