package de.thatsben.lobbysystem.listener;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.util.Vector;

public class DoubleJumpListener implements Listener
{

    @EventHandler
    public void onJoin(PlayerJoinEvent event)
    {
        event.getPlayer().setAllowFlight(true);
        event.getPlayer().setFlying(false);
    }

    @EventHandler
    public void onFly(PlayerToggleFlightEvent event)
    {
        Player player = event.getPlayer();
        if (player.getGameMode() == GameMode.ADVENTURE)
        {
            event.setCancelled(true);
            player.setAllowFlight(false);
            player.setFlying(false);
            Vector vector = new Vector(0, 0.1, 1);
            player.setVelocity(player.getLocation().getDirection().multiply(2).add(vector));
        }
    }

    @EventHandler
    public void onMove(PlayerMoveEvent event)
    {
        Player player = event.getPlayer();
        if (player.getGameMode() == GameMode.ADVENTURE)
        {
            if (player.getLocation().add(0, -2, 0).getBlock().getType() == Material.REDSTONE_BLOCK)
            {
                player.setAllowFlight(true);
                Vector vector = new Vector(0, 0.1, 1);
                player.setVelocity(player.getLocation().getDirection().multiply(2).add(vector));
            }
        }
    }
}
