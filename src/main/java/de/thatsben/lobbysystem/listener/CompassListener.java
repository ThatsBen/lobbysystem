package de.thatsben.lobbysystem.listener;

import de.thatsben.lobbysystem.utils.ItemBuilder;
import de.thatsben.lobbysystem.utils.Msg;
import de.thatsben.lobbysystem.warp.WarpManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;

public class CompassListener implements Listener
{

    /**
     * this method creates an inventory and fills it with items. 
     * @return the inventory when you open the navigator.
     */
    public Inventory get()
    {
        Inventory inventory = Bukkit.createInventory(null, 9 * 6, ChatColor.DARK_GRAY + "Wohin möchtest du?");

        for (int i = 0; i < inventory.getSize(); i++)
        {
            inventory.setItem(i, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short) 7).withName(" ").build());
        }

        inventory.setItem(11, new ItemBuilder(Material.BED).withName("§6§lBEDWARS").build());
        inventory.setItem(13, new ItemBuilder(Material.GOLDEN_APPLE).withName("§6§lCOMMUNITY").build());
        inventory.setItem(15, new ItemBuilder(Material.WEB).withName("§6§lCWBW-TRAINING").build());
        inventory.setItem(19, new ItemBuilder(Material.SANDSTONE).withName("§6§lBRIDGE").build());
        inventory.setItem(22, new ItemBuilder(Material.NETHER_STAR).withName("§6§lSPAWN").build());
        inventory.setItem(25, new ItemBuilder(Material.STICK).withName("§6§lMLG-RUSH").build());
        inventory.setItem(29, new ItemBuilder(Material.ANVIL).withName("§6§lTRAINING").build());
        inventory.setItem(33, new ItemBuilder(Material.DIAMOND_SWORD).withName("§6§lKIT-PVP").build());
        inventory.setItem(39, new ItemBuilder(Material.ENDER_PEARL).withName("§6§lFUN GAMES").build());
        inventory.setItem(41, new ItemBuilder(Material.WOOD_AXE).withName("§6§lGUNGAME").build());

        return inventory;
    }

    /**
     * this method opens the navigator inventory
     * @param event
     */
    @EventHandler
    public void onInteract(PlayerInteractEvent event)
    {
        Player player = event.getPlayer();
        
        if (event.getItem() == null) return;
        if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK)
        {
            if (event.getItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "Spielmodus auswählen"))
            {
                player.openInventory(get());
            }
        }
    }

    /**
     * This method contains the logic for clicking in the inventory to teleport to locations (warps)
     * @param event
     */
    @EventHandler
    public void onInventoryClick(InventoryClickEvent event)
    {
        Player player = (Player) event.getWhoClicked();
        if (event.getView().getTitle().equals(ChatColor.DARK_GRAY + "Wohin möchtest du?"))
        {
            event.setCancelled(true);

            try
            {
                switch (event.getSlot())
                {
                    case 11:
                        player.teleport(WarpManager.getWarp("bedwars"));
                        break;
                    case 13:
                        player.teleport(WarpManager.getWarp("community"));
                        break;
                    case 15:
                        player.teleport(WarpManager.getWarp("cwbwt"));
                        break;
                    case 19:
                        player.teleport(WarpManager.getWarp("bridge"));
                        break;
                    case 22:
                        player.teleport(WarpManager.getWarp("spawn"));
                        break;
                    case 25:
                        player.teleport(WarpManager.getWarp("mlgrush"));
                        break;
                    case 29:
                        player.teleport(WarpManager.getWarp("training"));
                        break;
                    case 33:
                        player.teleport(WarpManager.getWarp("kitpvp"));
                        break;
                    case 39:
                        player.teleport(WarpManager.getWarp("fungames"));
                        break;
                    case 41:
                        player.teleport(WarpManager.getWarp("gungame"));
                        break;
                    default:
                        break;
                }
            }
            catch (IllegalArgumentException e)
            {
                player.sendMessage(Msg.systemPrefix + ChatColor.RED + "Dieser Spawn wurde bisher nicht gesetzt. Bitte melde dich umgehend bei einem Teammitglied.");
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }
    
}
