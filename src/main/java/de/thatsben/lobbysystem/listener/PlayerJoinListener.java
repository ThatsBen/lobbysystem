package de.thatsben.lobbysystem.listener;

import de.thatsben.lobbysystem.utils.ItemBuilder;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinListener implements Listener
{

    /**
     * This method creates the inventory or puts the items into the inventory that you get during the server join
     * @param player who gets the inventory
     */
    public void joinInventory(Player player)
    {
        player.getInventory().setItem(0, new ItemBuilder(Material.COMPASS).withName(ChatColor.GOLD + "Spielmodus auswählen").build());
        player.getInventory().setItem(1, new ItemBuilder(Material.SANDSTONE, 64).withName(ChatColor.GREEN + "Blöcke").build());
        player.getInventory().setItem(4, new ItemBuilder(Material.NAME_TAG).withName(ChatColor.GOLD + "Nickname").build());
        player.getInventory().setItem(7, new ItemBuilder(Material.BLAZE_ROD).withName(ChatColor.GOLD + "Spieler-Sichtbarkeit").build());
        player.getInventory().setItem(8, new ItemBuilder(Material.NETHER_STAR).withName(ChatColor.GOLD + "Lobby Auswahl").build());
    }

    /**
     * This method does the things that happen when you join the server
     * @param event
     */
    @EventHandler
    public void handleJoin(PlayerJoinEvent event)
    {
        Player player = event.getPlayer();
        event.setJoinMessage(null);

        player.getInventory().clear();
        player.getInventory().setArmorContents(null);
        player.setExp(0);
        player.setLevel(0);

        joinInventory(player);
    }
}
