package de.thatsben.lobbysystem.listener;

import de.thatsben.lobbysystem.LobbySystem;
import de.thatsben.lobbysystem.utils.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;

public class HidePlayerListener implements Listener
{

    /**
     * this method creates an inventory and fills it with items. 
     * @return the inventory when you open the blaze_rod.
     */
    public Inventory get()
    {
        Inventory inventory = Bukkit.createInventory(null, 9 * 3, "§8Wen möchtest du sehen?");

        for (int i = 0; i < inventory.getSize(); i++)
        {
            inventory.setItem(i, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short) 7).withName(" ").build());
        }

        inventory.setItem(10, new ItemBuilder(Material.WOOL, 1, (short) 13).withName("§aJeden").build());
        inventory.setItem(13, new ItemBuilder(Material.WOOL, 1, (short) 1).withName("§eNur VIPs und Teammitglieder").build());
        inventory.setItem(16, new ItemBuilder(Material.WOOL, 1, (short) 14).withName("§cNiemanden").build());
        inventory.setItem(26, new ItemBuilder(Material.BARRIER).withName("§cInventar schließen").build());

        return inventory;
    }

    /**
     * this method opens the inventory for the interactions (which player you want to see)
     * @param event
     */
    @EventHandler
    public void onInteract(PlayerInteractEvent event)
    {
        Player player = event.getPlayer();
        if (event.getItem() == null) return;
        if (event.getItem().getItemMeta().getDisplayName().equals("§6Spieler-Sichtbarkeit"))
        {
            if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK)
            {
                player.openInventory(get());
            }
        }
    }

    /**
     * This method contains the logic for clicking in the inventory to decide what kind of player you want to see
     * @param event
     */
    @EventHandler
    public void onInventoryClick(InventoryClickEvent event)
    {
        Player player = (Player) event.getWhoClicked();
        if (event.getView().getTitle().equals(ChatColor.DARK_GRAY + "Wen möchtest du sehen?"))
        {
            event.setCancelled(true);
            if (event.getSlot() == 10) // everyone
            {
                for (Player all : Bukkit.getOnlinePlayers())
                {
                    player.showPlayer(all);
                    player.sendMessage(LobbySystem.getInstance().getPrefix() + ChatColor.GRAY + "Du siehst nun" + ChatColor.GREEN + " alle " + ChatColor.GRAY + "anderen Spieler.");
                }
                player.closeInventory();
            }

            if (event.getSlot() == 13) // vips and staff
            {
                for (Player all : Bukkit.getOnlinePlayers())
                {
                    if (all.hasPermission("lobby.playerhide.vips"))
                    {
                        player.showPlayer(all);
                        player.sendMessage(LobbySystem.getInstance().getPrefix() + ChatColor.GRAY + "Du siehst jetzt nur noch" + ChatColor.DARK_PURPLE + " VIPs" + ChatColor.GRAY + " und" + ChatColor.RED + " Teammitglieder" + ChatColor.GRAY + ".");
                    }
                }
                player.closeInventory();
            }

            if (event.getSlot() == 16) // nobody
            {
                for (Player all : Bukkit.getOnlinePlayers())
                {
                    player.hidePlayer(all);
                    player.sendMessage(LobbySystem.getInstance().getPrefix() + ChatColor.GRAY + "Du siehst nun" + ChatColor.RED + " niemanden" + ChatColor.GRAY + " mehr.");
                }
                player.closeInventory();
            }

            if (event.getSlot() == 26) // close button/item
            {
                player.closeInventory();
            }
        }
    }
}

