package de.thatsben.lobbysystem.listener;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class NicknameItemListener implements Listener
{

    /**
     * This method executes with right click the /nick command
     * @param event
     */
    @EventHandler
    public void onInteract(PlayerInteractEvent event)
    {
        Player player = event.getPlayer();
        if (event.getItem().getItemMeta() != null)
        {
            if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK)
            {
                if (event.getItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "Nickname"))
                {
                    player.performCommand("nick");
                }
            }
        }
    }
}
