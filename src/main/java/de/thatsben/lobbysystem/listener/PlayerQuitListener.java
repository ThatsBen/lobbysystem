package de.thatsben.lobbysystem.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuitListener implements Listener
{

    /**
     * This method blocks the default leave message when leaving the server
     * @param event
     */
    @EventHandler
    public void handleQuit(PlayerQuitEvent event)
    {
        event.setQuitMessage(null);
    }

    /**
     * This method blocks the default kick message when a player got kicked from the server
     * @param event
     */
    @EventHandler
    public void handleKick(PlayerKickEvent event) {
        event.setLeaveMessage(null);
    }
}
