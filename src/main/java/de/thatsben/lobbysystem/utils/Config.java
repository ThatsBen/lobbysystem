package de.thatsben.lobbysystem.utils;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Config
{

    // -- instances
    
    private FileConfiguration configuration;
    private File file;

    // -- constructor 
    
    /**
     * 
     * @param name
     * @param path
     */
    public Config(String name, File path)
    {
        file = new File(path, name);
        if (!file.exists()) 
        {
            try
            {
                file.createNewFile();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }

        configuration = new YamlConfiguration();
        try
        {
            configuration.load(file);
        }
        catch (FileNotFoundException | InvalidConfigurationException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
    
    // -- public methods

    /**
     * this method will save the config
     */
    public void save()
    {
        try
        {
            configuration.save(file);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * this method will reload the config
     */
    public void reload()
    {
        try
        {
            configuration.load(file);
        }
        catch (FileNotFoundException | InvalidConfigurationException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
    
    
    public boolean contains(String path)
    {
        return configuration.contains(path);
    }

    /**
     * 
     * @return true if the config is clear
     */
    public boolean isClear()
    {
        return configuration.getKeys(false).size() == 0;
    }
    
    // -- getter and setter

    public File getFile()
    {
        return file;
    }

    public FileConfiguration getConfiguration()
    {
        return configuration;
    }

    public Object get(String path)
    {
        return configuration.get(path);
    }

    public void set(String path, Object obj)
    {
        configuration.set(path, obj);
    }

}