package de.thatsben.lobbysystem.utils;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.List;

public class ItemBuilder
{

    // -- instances
    
    private Material material;
    private int amount;
    private int subid;
    private ItemMeta meta;
    private SkullMeta skullMeta;
    private ItemStack itemStack;
    private String id;

    // -- constructors 
    
    public ItemBuilder(Material material, int amount, int subid)
    {
        this.material = material;
        this.amount = amount;
        this.subid = subid;
        this.itemStack = new ItemStack(material, amount, (short)subid);
        this.meta = itemStack.getItemMeta();
    }

    public ItemBuilder(ItemStack itemStack)
    {
        this.material = itemStack.getType();
        this.amount = itemStack.getAmount();
        this.subid = itemStack.getTypeId();
        this.meta = itemStack.getItemMeta();
        this.itemStack = itemStack;
    }

    public ItemBuilder(Material material, int amount)
    {
        this.material = material;
        this.amount = amount;
        this.itemStack = new ItemStack(material, amount, (short)0);
        this.meta = itemStack.getItemMeta();
    }

    public ItemBuilder(Material material)
    {
        this.material = material;
        this.itemStack = new ItemStack(material);
        this.meta = itemStack.getItemMeta();
    }

    public ItemBuilder withName(String s)
    {
        this.meta.setDisplayName(s);
        return this;
    }

    public ItemBuilder withID(String id)
    {
        this.id = id;
        return this;
    }

    public ItemBuilder withLore(List<String> list)
    {
        this.meta.setLore(list);
        return this;
    }

    public ItemBuilder addEnchantment(Enchantment enchantment, int level)
    {
        this.meta.addEnchant(enchantment, level, true);
        return this;
    }
    
     public ItemBuilder setUnbreakable(boolean unbreakable)
     {
         this.meta.spigot().setUnbreakable(unbreakable);
         return this;
     }
     

    public ItemStack build()
    {
        this.itemStack.setItemMeta(this.meta);
        return this.itemStack;
    }

    public ItemBuilder setSkull(String displayname, String skullOwner, int amount)
    {
        itemStack = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
        skullMeta = (SkullMeta) itemStack.getItemMeta();
        skullMeta.setDisplayName(displayname);
        skullMeta.setOwner(skullOwner);
        return this;
    }

    public ItemStack buildSkull()
    {
        itemStack.setItemMeta(this.skullMeta);
        return itemStack;
    }

}
